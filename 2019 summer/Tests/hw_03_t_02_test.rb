require_relative 'test_helper'

if solution_file.any?
  class TestHW3 < Minitest::Test
    def test_true
      assert_equal true, true
    end

    def test_task_3_2_valid_config_full
      input = <<-YAML
        stage:
          adapter: mysql2
          encoding: utf8
          reconnect: false
          database: test-mysql2_development
          pool: 5
          timeout: 5000
          username: root
          password: password
          socket: /tmp/mysql.sock

        development:
          adapter: sqlite3
          database: db/development.sqlite3
          pool: 5
          timeout: 1000

        production:
          adapter: postgresql
          encoding: unicode
          database: test-postgres_production
          pool: 5
          username: test-postgres
          password: x123
      YAML

      assert_equal [
        { stage: { db: 'test-mysql2_development', user: 'root', password: 'password', magic_number: 25_000 } },
        { development: { db: 'db/development.sqlite3', magic_number: 5000 } },
        { production: { db: 'test-postgres_production', user: 'test-postgres', password: 'x123', magic_number: 5000 } }
      ], task_3_2(input), 'should extract the right data'
    end

    def test_task_3_2_empty_config
      assert_empty task_3_2(""), "should be empty for missing envs"
    end

    def test_task_3_2_default_values
      input = <<-YAML
        stage:
          adapter: mysql2
          encoding: utf8
          reconnect: false
          database: test-mysql2_development
          username: root
          password: password
          socket: /tmp/mysql.sock
      YAML

      assert_equal [
        { stage: { db: 'test-mysql2_development', user: 'root', password: 'password', magic_number: 1000 } }
      ], task_3_2(input), 'should use default values'
    end
  end
end
