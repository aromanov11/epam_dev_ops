require_relative 'test_helper'

if solution_file.any?
  class TestHW3 < Minitest::Test
    def test_true
      assert_equal true, true
    end

    def test_task_3_1_fib_numbers_zero
      assert_equal [0], task_3_1(0), 'should be 0 for 0'
    end

    def test_task_3_1_fib_numbers_invalid
      assert_equal [], task_3_1(-1), 'should be empty for invalid'
    end

    def test_task_3_1_fib_numbers_starts_from_one
      assert_equal [1], task_3_1(1), 'should start from 1'
    end

    def test_task_3_1_fib_numbers_corner_case
      assert_equal [1, 1], task_3_1(2), "should start from 1"
    end

    def test_task_3_1_fib_numbers_normal_sequence
      assert_equal [1, 1, 2, 3, 5, 8, 13, 21, 34, 55], task_3_1(10), 'should return the valid sequence'
    end

    def test_task_3_1_fib_numbers_big_number
      assert_equal task_3_1(998).size, 998, "should handle big numbers"
    end
  end
end
