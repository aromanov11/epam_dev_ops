require_relative 'test_helper'

if solution_file.any?
  class TestHW2 < Minitest::Test
    def test_true
      assert_equal true, true
    end

    def test_task_2_formating_valid
      input = <<~LOG
        10.6.246.103 - - [23/Apr/2018:20:30:39 +0300] "POST /test/2/messages HTTP/1.1" 200 48 0.0498
        10.6.246.101 - - [23/Apr/2018:20:30:42 +0300] "POST /test/2/run HTTP/1.1" 200 - 0.2277
        2018-04-23 20:30:42: SSL ERROR, peer: 10.6.246.101, peer cert: , #<Puma::MiniSSL::SSL: System error: Undefined error: 0 - 0>
        10.6.246.101 - - [23/Apr/2018:20:31:39 +0300] "POST /test/2/messages HTTP/1.1" 200 48 0.0290
      LOG

      assert_equal [
        '23/Apr/2018:20:30:39 +0300 FROM: 10.6.246.103 TO: /TEST/2/MESSAGES',
        '23/Apr/2018:20:30:42 +0300 FROM: 10.6.246.101 TO: /TEST/2/RUN',
        '23/Apr/2018:20:31:39 +0300 FROM: 10.6.246.101 TO: /TEST/2/MESSAGES'
      ], task_2_2(input), 'should extract valid messages from successfull log entries'
    end

    def test_task_2_formating_no_records
      input = '10.6.246.103 - - [23/Apr/2018:20:30:39 +0300] "POST HTTP/1.1" 200 48 0.049'

      assert_empty task_2_2(input), 'should be empty for bad records'
    end

    def test_task_2_formating_only_errors
      input = <<~LOG
        2018-04-23 20:30:42: SSL ERROR, peer: 10.6.246.101, peer cert: , #<Puma::MiniSSL::SSL: System error: Undefined error: 0 - 0>
        2018-04-23 20:30:52: SSL error, peer: 10.6.246.101, peer cert: , #<Puma::MiniSSL::SSL: System error: Undefined error: 0 - 0>
      LOG

      assert_empty task_2_2(input), 'should be empty for errors'
    end
  end
end
