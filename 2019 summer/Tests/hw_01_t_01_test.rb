require_relative 'test_helper'

if solution_file.any?
  class TestHW1 < Minitest::Test
    def test_true
      assert_equal true, true
    end

    def test_task_1_negative
      refute task_1_1('hello'), "shouldn't detect palindrome correctly"
    end

    def test_task_1_positive
      assert task_1_1('sum summus mus'), 'should detect palindrome correctly'
    end

    def test_task_1_one
      assert task_1_1('1'), 'should detect palindrome correctly'
    end

    def test_task_1_camelcase
      assert task_1_1('sUm suMMus Mus'), 'should detect palindrome correctly'
    end
  end
end
