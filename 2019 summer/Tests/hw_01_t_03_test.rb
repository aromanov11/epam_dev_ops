require_relative 'test_helper'

if solution_file.any?
  class TestHW1 < Minitest::Test
    def test_task_3_sum
      input = '2018-04-24 17:03:45.6 ubuntu-xenial[17425] Information - Detected docker: Version: 17.05.0-ce, API Version: v1.29'

      assert_equal 40, task_1_3(input)
    end

    def test_task_3_sum_is_zero
      input = ''

      assert_equal 0, task_1_3(input)
    end
  end
end
