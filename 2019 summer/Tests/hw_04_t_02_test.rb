require_relative "test_helper"

if solution_file.any?
  class TestHW4 < Minitest::Test
    def test_true
      assert_equal true, true
    end

    def test_task_4_2_sum
      assert_equal 10, task_4_2([1, 3, 6]), "should calculate sum"
    end

    def test_task_4_2_sum_with_default
      assert_equal 19, task_4_2([1, 3, 6], 9), "should calculate sum with default"
    end

    def test_task_4_2_sum_with_block
      got = task_4_2([1, 3, 6]) { |e| e % 2 }
      assert_equal 2, got, "should calculate sum with block"
    end

    def test_task_4_2_sum_with_block_float
      got = task_4_2([1, 3, 6]) { |e| e + 0.1 }
      assert_equal 10.3, got, "should calculate sum with block"
    end

    def test_task_4_2_sum_with_block_and_default
      got = task_4_2([1, 3, 6], 8) { |e| e % 2 }
      assert_equal 10, got, "should calculate sum with block"
    end
  end
end
