require_relative 'test_helper'

if solution_file.any?
  class TestHW3 < Minitest::Test
    def test_true
      assert_equal true, true
    end

    def test_task_3_3_middle
      assert_equal 556, task_3_3((555..777)), 'should find the required number'
    end

    def test_task_3_3_small
      assert_equal 101, task_3_3((1..777)), 'should find the first required number'
    end

    def test_task_3_3_negative
      assert_equal -550, task_3_3((-555..777)), 'should find the first required number'
    end

    def test_task_3_3_big
      assert_equal 997, task_3_3((991..10_000)), 'should find the required number'
    end

    def test_task_3_3_one
      assert_equal 101, task_3_3((101..101)), 'should find the required number'
    end

    def test_task_3_3_one_2
      assert_equal 101, task_3_3((101..102)), 'should find the required number'
    end

    def test_task_3_not_three_digit_number
      assert_nil task_3_3(10..99), 'should return nil'
    end
  end
end
