require_relative 'test_helper'
require 'timeout'

if solution_file.any?
  class TestHW4 < Minitest::Test
    def test_true
      assert_equal true, true
    end

    def test_task_4_1_bench_slow
      slow = proc { sleep(2) }
      Timeout.timeout(5) do
        assert_equal 2.0, task_4_1(slow), 'should floor accuratelly'
      end
    end

    def test_task_4_1_bench_fast
      fast = proc { 1 }
      Timeout.timeout(5) do
        assert_equal 0.0, task_4_1(fast), 'should floor accuratelly'
      end
    end

    def test_task_4_1_bench_accuracy_asc
      slow = proc { sleep(1.59999) }
      Timeout.timeout(5) do
        assert_equal 1.6, task_4_1(slow), 'should floor accuratelly'
      end
    end

    def test_task_4_1_bench_accuracy_desc
      slow = proc { sleep(0.544) }
      Timeout.timeout(5) do
        assert_equal 0.5, task_4_1(slow), 'should floor accuratelly'
      end
    end
  end
end
