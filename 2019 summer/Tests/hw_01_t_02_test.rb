require_relative 'test_helper'

if solution_file.any?
  class TestHW1 < Minitest::Test
    def test_task_2_sum_of_digits
      input = '2018-04-24 17:03:45.6 ubuntu-xenial[17425] Information - Detected docker: Version: 17.05.0-ce, API Version: v1.29'

      assert_equal 91, task_1_2(input)
    end

    def test_task_2_sum_is_zero_for_no_numbers
      input = 'this is cool log'

      assert_nil task_1_2(input)
    end
  end
end
