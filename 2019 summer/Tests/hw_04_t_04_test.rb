require_relative 'test_helper'

if solution_file.any?
  class TestHW4 < Minitest::Test
    def test_true
      assert_equal true, true
    end

    def test_task_4_4_no_block
      assert_equal 'ERROR', task_4_4(1, 1), 'should return an error'
    end

    def test_task_4_4_no_block_many
      assert_equal 'ERROR', task_4_4(1, 1, 1, 1, 1, 1, 1), 'should return an error'
    end

    def test_task_4_4_with_block
      string_io = StringIO.new
      $stdout = string_io
      task_4_4(5, 5, 1, 2) { |e| puts e }
      $stdout = STDOUT
      assert_equal "1\n2\n", string_io.string, 'should call a block'
    end

    def test_task_4_4_with_block_2
      init = %w[a b c d e]
      task_4_4(*init, &:upcase!)
      assert_equal %w[a b C D E], init, 'should call a block'
    end

    def test_task_4_4_with_block_3
      string_io = StringIO.new
      $stdout = string_io
      task_4_4(5, 5) { |e| puts e }
      $stdout = STDOUT
      assert_equal '', string_io.string, 'should call a block'
    end
  end
end
