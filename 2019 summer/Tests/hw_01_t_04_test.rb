require_relative 'test_helper'

if solution_file.any?
  class TestHW1 < Minitest::Test
    def test_task_4_0_0
      h = 0
      m = 0

      assert_equal 0, task_1_4(h, m)
    end

    def test_task_4_7_5
      h = 15
      m = 15

      assert_equal 7.5, task_1_4(h, m)
    end

    def test_task_12_30
      h = 12
      m = 30

      assert_equal 165, task_1_4(h, m)
    end

    def test_task_3_30
      h = 3
      m = 30

      assert_equal 75, task_1_4(h, m)
    end
  end
end
