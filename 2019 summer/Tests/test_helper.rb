require 'stringio'
require 'minitest/autorun'
require 'minitest/pride'
require 'json'

def solution_file
  Dir[File.join(__dir__, '../Solutions', '*.rb')].each { |file| require file }
end
