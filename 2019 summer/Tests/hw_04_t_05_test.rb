require_relative 'test_helper'

if solution_file.any?
  class TestHW4 < Minitest::Test
    def test_true
      assert_equal true, true
    end

    def test_task_4_5_empty_even
      assert_equal [], task_4_5([1]) { |_e| true }, 'should be empty'
    end

    def test_task_4_5_empty_condition
      assert_equal [], task_4_5([1, 2, 3]) { |_e| false }, 'should be empty'
    end

    def test_task_4_5_not_empty
      assert_equal [6], task_4_5([1, 2, 4, 5, 6, 7, 8, 9, 10]) { |e| e > 4 && e < 8 }, 'should not be empty'
    end
  end
end
