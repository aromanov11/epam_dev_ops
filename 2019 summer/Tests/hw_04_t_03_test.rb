require_relative 'test_helper'

if solution_file.any?
  class TestHW4 < Minitest::Test
    def test_true
      assert_equal true, true
    end

    def test_task_4_3_empty
      assert_equal '', task_4_3(''), 'empty for empty'
    end

    def test_task_4_3_not_block
      assert_equal "hello\nworld", task_4_3("hello\nworld"), 'empty for missing block'
    end

    def test_task_4_3_with_block_empty
      assert_equal [], task_4_3('') { |l| l.include? 'e' }, 'empty for empty'
    end

    def test_task_4_3_with_block_filter
      assert_equal [true, false], task_4_3("hello\nworld") { |line| line.include? 'e' }, 'should classify results'
    end

    def test_task_4_3_with_block_filter_take
      assert_equal %w[h m c w], task_4_3("hello\nmy\ncruel\nworld") { |line| line[0] }, 'should classify results'
    end
  end
end
