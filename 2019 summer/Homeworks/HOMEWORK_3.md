# Домашнее задание: 3

## Практика

- Практическая часть должна быть сдана до начала следующей лекции
- Для выполнения практической части должен использоваться `Ruby >= 2.5.1`
- Практическая часть должна быть отправлена на почту `aliaksei_ramanau@epam.com`
- На каждое задание должен быть отдельный файл. 
- Файл должен называться по шаблону `name_surname_hw_03_t_01.rb`, например `aliaksei_ramanau_hw_03_t_01.rb`
- для второго задания соответственно: `name_surname_hw_03_t_02.rb`, например `aliaksei_ramanau_hw_03_t_02.rb`
- и т.д.
- дополнительным плюсом будет зеленый `rubocop` ссылка на стаил гайды - https://github.com/rubocop-hq/ruby-style-guide

### Задания

#### 1
`def task_3_1(n)`: написать метод, который выводит последовательность чисел [Фибонначи](https://en.wikipedia.org/wiki/Fibonacci_number) для переданного числа n:  
n - аргумент, указывающий на число элементов в последовательности  

Начиная с 1. Выводит 0, если передан 0.
Максимальное число 1000

Пример работы:

```ruby
task_3_1(5) # => 1, 1, 2, 3, 5
```

#### 2
`def task_3_2(input)`: дан текст в формате YAML (http://www.yaml.org/).
Распарсить его вручную или используя http://ruby-doc.org/stdlib-2.5.1/libdoc/yaml/rdoc/YAML.html. Формат файл следующий (начиная с # - мои комменты):

```yaml
stage: # название env
  adapter: mysql2 # не обязательный
  encoding: utf8  # не обязательный
  reconnect: false # не обязательный
  database: test-mysql2_development # обязательный
  pool: 5 # не обязательный, считаем 1 по-умолчанию
  timeout: 5000 # не обязательный, считаем 1000 по-умолчанию
  username: root # не обязательный
  password: password # не обязательный
  socket: /tmp/mysql.sock # не обязательный

development:
  adapter: sqlite3
  database: db/development.sqlite3
  pool: 5
  timeout: 1000

production:
  adapter: postgresql
  encoding: unicode
  database: test-postgres_production
  pool: 5
  username: test-postgres
  password: x123
```

Необходимо вернуть массив всех конфигураций (пустой, если их нет), каждая в виде хэша следующего формата:
```ruby
{
  env_name: {
    db: database value,
    user: username value если есть,
    password: password value если есть,
    magic_number: pool * timeout, используя значения по-умолчанию,если они отсутствуют
  }
}
```

Для примера выше это будет:

```ruby
[
  { stage: { db: "test-mysql2_development", user: "root", password: "password", magic_number: 25000 } },
  { development: { db: "db/development.sqlite3", magic_number: 5000 } },
  { production: { db: "test-postgres_production", user: "test-postgres", password: "x123", magic_number: 5000 } },
 ]
 ```
#### 3
`def task_3_3(range)`: дан диапазон `range`, найти первое трёх­значное число у которого остаток деления на 7 равен 3

