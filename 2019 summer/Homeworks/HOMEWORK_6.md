# Домашнее задание: 6*(дополнительное)

## Практика

- Для выполнения практической части должен использоваться `Ruby >= 2.5.1`
- Практическая часть должна быть отправлена на почту `aliaksei_ramanau@epam.com`
- Файлы должны называться по шаблону `name_surname_hw_06_life.rb'
- дополнительным плюсом будет зеленый `rubocop` ссылка на стаил гайды - https://github.com/rubocop-hq/ruby-style-guide

### Задание

Написать игру жизнь:

https://ru.wikipedia.org/wiki/%D0%98%D0%B3%D1%80%D0%B0_%C2%AB%D0%96%D0%B8%D0%B7%D0%BD%D1%8C%C2%BB
https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life

Использовать ООП (разбить по классам)

Возможность запуска с заранее установленными значениями:

1. https://en.wikipedia.org/wiki/File:Game_of_life_pulsar.gif
2. https://upload.wikimedia.org/wikipedia/commons/f/f2/Game_of_life_animated_glider.gif
