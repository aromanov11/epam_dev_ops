module Cappuccinator
  def create_foam
    prepare_milk
    push_foam
  end

  private

  def prepare_milk
    puts "Отбираем и кипятим молоко"
  end

  def push_foam
    puts "Выпускаем молочную пенку в чашку"
  end
end

class CoffeeMachine
  attr_accessor :water_value
  def initialize(water_value)
    @water_value = water_value
  end

  def make_coffee
    # puts "Готовим воду и зёрна"
    # puts "Варим и наливаем кофе"
    get_water(water_value)  # набираем воду
    get_beans(50)   # набираем зёрна
    prepare_beans   # готовим зёрна
    boil_water      # кипятим воду
    pour_coffee     # наливаем кофе в чашку
  end

  private

  def get_water(mls)
    puts "Набираем в ёмкость #{mls} мл воды."
  end

  def get_beans(grams)
    puts "Отбираем из контейнера #{grams} г зёрен кофе."
  end

  def prepare_beans
    puts "Готовим Зерна"
  end

  def boil_water
    puts "Кипятим воду"
  end

  def pour_coffee
    puts "Наливаем кофе в чашку"
  end
end

class CappuccinoMachine < CoffeeMachine
  include Cappuccinator
  include Cappuccinator
  def make_coffee
    super
    create_foam
  end
end

class CapsuleMachine < CoffeeMachine
  def make_coffee
    get_water(200)  # набираем воду
    prepare_capsule
    # get_beans(50)   # набираем зёрна
    # prepare_beans   # готовим зёрна
    boil_water      # кипятим воду
    pour_coffee     # наливаем кофе в чашку
  end
  private 

  def prepare_capsule
    puts "Вскрываем капсулу и высыпаем кофе в емкость."
  end

end

class CapsuleCappuccino < CapsuleMachine
  include Cappuccinator
  def make_coffee
    super
    create_foam
  end
end
# capsule = CapsuleMachine.new
saeco = CoffeeMachine.new(300)
# saeco.make_coffee
# capuchino = CappuccinoMachine.new
# cc = CapsuleCappuccino.new
require 'pry'; binding.pry
