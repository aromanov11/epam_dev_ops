# puts "Hello"

# 10.times do |el|
#   begin
#     puts el
#     # raise if el == 8
#     puts 1/(el -8)

#     # Array.hello
#   rescue RuntimeError => error
#     puts "My error"
#     puts error.inspect
#     puts error.message
#     puts error.backtrace

#   end
#   # ensure
#   #   puts "Safe exit"
# end
class MyError < StandardError
  def message
    "MY ERROR!"
  end
end
raise MyError
