class Gladiator
  attr_accessor :name
  def initialize(name)
    @name = name
  end
  
  def say_hello(l, l2)
  # def say_hello
    puts "Поклон тебе, Цезарь!"
    l.call
    l2.call
    yield if block_given?
    # yield if block_given?
    puts "Идущие на смерть приветствуют тебя!"
  end
end
# name = "Abc"
spartak = Gladiator.new("Спартак")
# max = Gladiator.new("Max")
# require 'pry'; binding.pry
# spartak.say_hello("Peter") { |name| puts " Я, #{name}, Составляю завещание!" }
# l = lambda {puts "Составляю завещание!"}
# l2 = lambda {puts "Подписываю завещание!"}
l = Proc.new {puts "Составляю завещание! Proc"}
l2 = Proc.new {puts "Подписываю завещание! Proc"}
spartak.say_hello(l,l2)
# spartak.say_hello {puts 1} {puts 2}
# max.say_hello { |name| puts " Я, #{name}, Составляю завещание!" }
#
# say_hello { pus "анимация поклона!" }
# say_hello 
