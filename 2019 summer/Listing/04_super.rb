
 class OldMan
   def say_wisdom
    puts "Народу много, а людей немного."
   end
end

class Man < OldMan
 def say_wisdom
  super
   puts "Так мой предок говаривал"
 end
end
 # => nil
 man = Man.new
 # => #<Man:0x88a0d9c>
 man.say_wisdom
