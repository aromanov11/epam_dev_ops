array = [1, 2, 3, 4]
def mega_sum(array, b = 0)
 array.each {|i| b = b + yield(i).to_i if block_given?}
 result = array.sum + b
end
p mega_sum(array)
p mega_sum(array, 10)
p mega_sum(array ) { |i| i ** 2 }
