class Test
  def self.get_test
    new
  end

  def show_test
    puts "show_test"
    self
  end

  def super_print
    puts "super_print"
  end
end

Test.get_test.show_test.super_print
require 'pry'; binding.pry
Test.get_test.show_test.super_print
