
module Cappuccinator
  def create_foam
    prepare_milk
    push_foam
  end

  private

  def prepare_milk
    puts "Отбираем и кипятим молоко"
  end

  def push_foam
    puts "Выпускаем молочную пенку в чашку"
  end
end
class CapsuleCappuccino < CapsuleMachine
  include Cappuccinator

  def push_foam
    puts "Красивыми узорами выкладываем пену в чашку."
  end

  private :push_foam
end
module LatteArt
  private

  def push_foam
    puts "Красивыми узорами выкладываем пену в чашку."
  end
end
class CapsuleCappuccino < CapsuleMachine
  include Cappuccinator
  include LatteArt
end
