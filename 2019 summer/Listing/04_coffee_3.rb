
 class Cleaner
   def clean(machine)
     machine.get_water(200)
    machine.boil_water
    machine.pour_coffee
   end
 end

 class MachineCleaner < CoffeeMachine
  def clean(machine)
    machine.get_water(200)
     machine.boil_water
     machine.pour_coffee
   end
 end

 cleaner = Cleaner.new
 # => #<Cleaner:0x8dbc2cc>
 machine_cleaner = MachineCleaner.new
 # => #<MachineCleaner:0x8e2450c>
cleaner.clean(saeco)
# NoMethodError: protected method 'get_water' called for #<CoffeeMachine:0x8df6940>
# :051 > machine_cleaner.clean(saeco)
