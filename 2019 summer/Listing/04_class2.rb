RUBY = 1

class B
  def x(a)
    p a
  end
end

def x
  B
end

class A < x
  def x
    p 'A'
  end
end

A.new.x
p A.ancestors
p A.superclass
