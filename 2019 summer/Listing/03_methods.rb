# def my_method(a, b = 1, *args, aa:, bb: 10, **kwargs, &block)
#   p "my_method args:"
#   # require 'pry'; binding.pry
#   p a, b, args, aa, bb, kwargs, block, block.call
# end
# my_method("a", "b", "c", "d", "e", "f", aa: "aa", cc: "cc", dd: "dd") {p "it's a block"}

def my(**my_args)
  puts my_args
end

my(a:1, b:2, c:3)

# def my_block(&block)
#   block.call()
# end
# my_block {puts "Завещание составлено" }



# def my_args(*args)
#   # x(args[0], args[1])
#   # require 'pry'; binding.pry
#   x(*args)
# end

# def x(a,b)
#   p "from x"
#   p a,b
# end
# my_args(1,2)



# def my_args(**args)
#   # x(args[0], args[1])
#   # require 'pry'; binding.pry
#   x(**args)
# end

# def x(a: ,b: )
#   p "from x"
#   p a,b
# end
# my_args(a: "a", b: "b")



# def my_block2(&block)
#   y(&block)
# end

# def y
#   p "from y"
#   yield
# end

# my_block2 {p 1}


