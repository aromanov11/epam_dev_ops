# a = [1,5,0,9]
# p a, "before"
#
# call = proc do
#   a = 10
#   p a
# end
#
# call.call
#
# p a, "after"



# # don't change
#
# a = [1,5,0,9]
# p a, "before"
#
# call = proc do |;a|
#   a = 10
#   p a
# end
#
# # теперь а не будет меняться
#
# call.call
#
# p a, "after"
